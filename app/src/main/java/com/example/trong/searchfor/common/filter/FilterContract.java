package com.example.trong.searchfor.common.filter;


import com.example.trong.searchfor.Interface.BasePresenter;
import com.example.trong.searchfor.Interface.BaseView;

import java.util.List;


public interface FilterContract {
  interface View extends BaseView<Presenter> {

  }
  interface Presenter extends BasePresenter {
    List<FilterItem> getFilteredCategories();
  }

  interface FilterView{
    void onFilterDialogClose(boolean applyFilter);
  }
}
