package com.example.trong.searchfor.view.place;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.esri.arcgisruntime.geometry.Envelope;
import com.example.trong.searchfor.R;
import com.example.trong.searchfor.utils.Constant;
import com.example.trong.searchfor.utils.Util;
import com.example.trong.searchfor.view.base.BaseActivity;
import com.example.trong.searchfor.view.map.MapActivity;


public class ListPlaceActivity extends BaseActivity {
    Toolbar toolbar;
    public LinearLayout linearLayout = null;
    private PlacesPresenter mPresenter = null;


    @Override
    protected int getLayoutResources() {
        return R.layout.activity_list_place;
    }

    @Override
    protected void initVariables(Bundle savedInstanceState) {
        toolbar = findViewById(R.id.toolbar);
        linearLayout = findViewById(R.id.llPlaceActivity);
        initToolbar();

    }

    @Override
    protected void initData(Bundle savedInstanceState) {
    }


    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if (requestCode == Constant.REQUEST_WIFI_SETTINGS || requestCode == Constant.REQUEST_LOCATION_SETTINGS) {
            checkSettings();
        }

    }

    public void initToolbar(){
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("Trang chủ");
        }
    }

    private void checkSettings() {
        boolean gpsEnabled = Util.locationTrackingEnabled(getApplicationContext());
        boolean internetConnected = Util.internetConnectivity(getApplicationContext());
        if (gpsEnabled && internetConnected) {
            requestLocationPermission();

        }else if (!gpsEnabled) {
            final Intent gpsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            showDialog(gpsIntent, Constant.REQUEST_LOCATION_SETTINGS, getString(R.string.location_tracking_off));
        }else if(!internetConnected)	{
            final Intent internetIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
            showDialog(internetIntent, Constant.REQUEST_WIFI_SETTINGS, getString(R.string.wireless_off));
        }
    }


    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            Snackbar.make(linearLayout, "Location access is required to search for places nearby.", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(final View view) {
                            // Request the permission
                            ActivityCompat.requestPermissions(ListPlaceActivity.this,
                                    new String[]{ Manifest.permission.ACCESS_FINE_LOCATION},
                                    Constant.PERMISSION_REQUEST_LOCATION);
                        }
                    }).show();

        } else {

            ActivityCompat.requestPermissions(this, new String[]{ Manifest.permission.ACCESS_FINE_LOCATION},
                    Constant.PERMISSION_REQUEST_LOCATION);
        }
    }
    @Override
    public final void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions,
                                                 @NonNull final int[] grantResults) {

        if (requestCode == Constant.PERMISSION_REQUEST_LOCATION) {
            if (grantResults.length != 1 ) {
                // Permission request was denied.
                Snackbar.make(linearLayout, R.string.locatin_permission, Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    public static Intent createMapIntent(Activity a, final Envelope envelope){
        Intent intent = new Intent(a, MapActivity.class);
        // Get the extent of search results so map
        // can set viewpoint

        if (envelope != null){
            intent.putExtra("MIN_X", envelope.getXMin());
            intent.putExtra("MIN_Y", envelope.getYMin());
            intent.putExtra("MAX_X", envelope.getXMax());
            intent.putExtra("MAX_Y", envelope.getYMax());
            intent.putExtra("SR", envelope.getSpatialReference().getWKText());
        }
        return  intent;
    }
    private void showMap(){
        final Envelope envelope = mPresenter.getExtentForNearbyPlaces();
        final Intent intent = createMapIntent(this, envelope);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            super.onBackPressed();
        } else {
            finish();
        }
    }

    private void showDialog(final Intent intent, final int requestCode, String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                //
                startActivityForResult(intent, requestCode);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(final DialogInterface dialog, final int which) {
                finish();
            }
        });
        alertDialog.create().show();
    }

}
